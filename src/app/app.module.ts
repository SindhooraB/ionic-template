import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { IonicStorageModule } from '@ionic/storage';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module'; 

import { PipesModule } from '@pipes/pipes.module';

@NgModule({
    declarations: [AppComponent],
    entryComponents: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
    imports: [
        BrowserModule, 
        IonicModule.forRoot({
            scrollPadding: false,
            scrollAssist: true
        }), 
        AppRoutingModule,
        HttpClientModule,
        PipesModule,
        IonicStorageModule.forRoot()
    ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
