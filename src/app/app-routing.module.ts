import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
   { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
   { path: 'dashboard', loadChildren: './pages/dashboard/dashboard.module#DashboardPageModule'},
   { path: 'account', loadChildren: './pages/account/account.module#AccountPageModule'},
   { path: 'onboarding', loadChildren: './pages/onboarding/onboarding.module#OnboardingPageModule'}
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
