import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Routes, RouterModule } from '@angular/router';

import { MyprofilePage } from './myprofile.page';

const routes: Routes = [
	{ path: '', component: MyprofilePage }
];

@NgModule({
	imports: [
    	CommonModule,
    	FormsModule,
    	IonicModule,
    	RouterModule.forChild(routes)
  	],
  	declarations: [MyprofilePage]
})
export class MyprofilePageModule {}
