import { SafePipe } from './safepipe.pipe';
import { DomSanitizer} from '@angular/platform-browser';
import { TestBed } from '@angular/core/testing';

describe('SafePipe', () => {
  let pipe: SafePipe;

  beforeEach(() => {
    const domsanSpy = jasmine.createSpyObj("DomSanitizer", ["bypassSecurityTrustResourceUrl"]);
    TestBed.configureTestingModule({
      providers: [
        SafePipe,
        { provide: DomSanitizer, useValue: domsanSpy }
      ]
    });
    pipe = TestBed.get(SafePipe);
  });


  it('create an instance', () => {
    expect(pipe).toBeTruthy();
  });
});
